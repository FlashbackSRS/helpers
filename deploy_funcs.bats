. ./deploy_funcs.sh

@test "default: default value" {
    env=PRODUCTION
    [ "$(default 1 FOO ${env})" == "1" ]
}

@test "default: global value" {
    FOO=5
    env=PRODUCTION
    [ "$(default 1 FOO ${env})" == "5" ]
}

@test "default: environment value" {
    FOO=5
    PRODUCTION_FOO=100
    env=PRODUCTION
    [ "$(default 1 FOO ${env})" == "100" ]
}

@test "default: no default value" {
    run default "" FOO PRODUCTION
    [ "$status" -eq 1 ]
    [ "$output" == "No default value for FOO" ]
}

@test "extract_host" {
    run extract_host "https://app.flashbacksrs.com/"
    [ "$output" == "app.flashbacksrs.com" ]
}

SAFEWORD="NO"MERGE # construct the safeword without triggering the test

@test "$SAFEWORD found in commit message" {
    tar -C "${BATS_TMPDIR}" -xzf ./testdata/nomerge-commit.tgz
    run post_test "${BATS_TMPDIR}/nomerge-commit"
    [ "$status" -eq 1 ]
    [ "${lines[0]}" == "$SAFEWORD found in commit message; merging not permitted" ]
}

@test "$SAFEWORD found in file contents" {
    tar -C "${BATS_TMPDIR}" -xzf ./testdata/nomerge-content.tgz
    run post_test "${BATS_TMPDIR}/nomerge-content"
    [ "$status" -eq 1 ]
    [ "${lines[0]}" == "$SAFEWORD found; merging not permitted" ]
}

@test "$SAFEWORD not found" {
    tar -C "${BATS_TMPDIR}" -xzf ./testdata/nomerge-notfound.tgz
    run post_test "${BATS_TMPDIR}/nomerge-notfound"
    [ "$status" -eq 0 ]
    [ "$output" == "" ]
}

@test "debug commit message" {
    tar -C "${BATS_TMPDIR}" -xzf ./testdata/debug-commit.tgz
    run post_test "${BATS_TMPDIR}/debug-commit"
    [ "$status" -eq 1 ]
    [ "${lines[0]}" == "'debug' commit message found; merging not permitted" ]
}
