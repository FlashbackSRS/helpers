Add this to the top of your `.gitlab-ci.yml` file:

    include:
      - "https://gitlab.com/FlashbackSRS/helpers/raw/master/default-gitlab-ci.yml"
      - "https://gitlab.com/FlashbackSRS/helpers/raw/master/review.yaml"
      - "https://gitlab.com/FlashbackSRS/helpers/raw/master/extra.yaml"
      - "https://gitlab.com/FlashbackSRS/helpers/raw/master/production.yaml"

defaut-gitlab-ci.yml
--------------------
This adds config that should be common to all projects. Specifically, it adds:

- A common `before_script`, which installs common shell functions used by other
  stages
- A `post-test` stage, which should be included after all test stages, and just
  before any production deployment stages. It runs a final sanity check, and
  fails if any debug-like commits are found.

review.yaml
-----------
This adds two stages: `review`, which deploys the app to the test cluster for
review, and `cleanup`, which cleans up the review environment.  To pass custom
configuration to the review deployment, it should be sufficient to redefine
just the review script as so:

    review:
      script:
        - full_deploy --set some.value="${SOME_VALUE}"

extra.yaml
----------
This adds three stages, which are part of AutoDevOps, but which I find to be of
dubious or rare value:

- `performance`
- `sast:container`
- `dast`

If you wish to use only a subset of these stages, you may disable the unwanted
stages as so:

    dast:
      only:
        variables:
         - - $SOME_VALUE_THAT_IS_NEVER_TRUE

This will only enable the stage when `$SOME_VALUE_THAT_IS_NEVER_TRUE` is set,
meaning never.

Debugging
---------

By setting the `FORCE_REFRESH` variable, you can force `deploy_funcs.sh` to be
downloaded in each stage. This can be useful for debugging a particular stage,
without rerunning all previous stages.
