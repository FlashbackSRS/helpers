#!/bin/bash

[[ "$TRACE" ]] && set -x
export CI_APPLICATION_REPOSITORY=$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
export CI_APPLICATION_TAG=$CI_COMMIT_SHA

TMP=$(mktemp -d)
trap 'rm -rf "${TMP}"' EXIT

function heading() {
    on='\033[0;36m'
    off='\033[0m'
    printf "${on}%s${off}\n" "$*"
}
function announce() {
    heading ${1}
    eval "$@"
}

function version { echo "$@" | awk '{split($0,a,"."); printf "%03d%03d%03d", a[1],a[2],a[3]}'; }
function version_gt() { test $(version "$1") -gt $(version "$2"); }
function ancestor() { git -C "${1:-$(pwd)}" merge-base HEAD refs/remotes/origin/master 2>/dev/null && true; }

function sast_container() {
    if [[ -n "$CI_REGISTRY_USER" ]]; then
        echo "Logging to GitLab Container Registry with CI credentials..."
        docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
        echo ""
    fi

    docker run -d --name db arminc/clair-db:latest
    docker run -p 6060:6060 --link db:postgres -d --name clair arminc/clair-local-scan:v2.0.1
    apk add -U wget ca-certificates
    docker pull ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}
    wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
    mv clair-scanner_linux_amd64 clair-scanner
    chmod +x clair-scanner
    touch clair-whitelist.yml
    retries=0
    echo "Waiting for clair daemon to start"
    while( ! wget -T 10 -q -O /dev/null http://docker:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(($retries+1)) ; done
    ./clair-scanner -c http://docker:6060 --ip $(hostname -i) -r gl-sast-container-report.json -l clair.log -w clair-whitelist.yml ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG} || true
}

function default() {
    def=$1
    var=$2
    env=$3
    eval val=\$${env}_${var}
    if [ -n "$val" ]; then
        echo "$val"
        return
    fi
    eval val=\$$var
    if [ -n "$val" ]; then
        echo "$val"
        return
    fi
    if [ -z "$def" ]; then
        echo "No default value for $var"
        return 1
    fi
    echo "$def"
}

function extract_host() {
    echo "$1" | awk -F[/:] '{print $4}'
}

function gcloud_setup() {
    case "$CI_ENVIRONMENT_NAME" in
        "review/"*)
            KEY=$GCP_ACCESS_KEY_REVIEW
            PROJECT_ID=$GCP_PROJECT_REVIEW
            ;;
        *)
            KEY=$GCP_ACCESS_KEY
            PROJECT_ID=$GCP_PROJECT
            ;;
    esac

    key="${TMP}/service_account.json"
    echo ${KEY} | base64 -d > "${key}"
    gcloud config set project ${PROJECT_ID}
    gcloud config set compute/zone ${GCP_REGION}-b
    gcloud config set compute/region ${GCP_REGION}
    gcloud auth activate-service-account --key-file "${key}"

    if gcloud container clusters get-credentials ${CLUSTER_NAME}; then
        echo "Authenticated to target cluster"
    fi
}

function ensure_cluster() {
    case "$CI_ENVIRONMENT_NAME" in
        "review/"*)
        echo "Starting review cluster ..."
            launch_review_cluster
            ;;
        *)
            ;;
    esac
}

function ensure_ingress() {
    helm repo add stable https://kubernetes-charts.storage.googleapis.com/
    helm repo update

    loadBalanceConf=""
    if [[ -n "$(helm ls -q "^nginx-ingress$" --tls)" ]]; then
        return
    fi

    EXTERNAL_IP=$(host foo.dev.flashbacksrs.com | awk '/has address/ { print $4; exit }')

    helm upgrade --install \
        --wait \
        --version 1.26.0 \
        --set rbac.create=true \
        --set controller.service.loadBalancerIP="${EXTERNAL_IP}" \
        --set controller.publishService.enabled=true \
        nginx-ingress \
        stable/nginx-ingress \
        --tls
}

function ensure_cert_manager() {
    helm repo add jetstack https://charts.jetstack.io
    helm repo update

    ver=0.11.0

    kubectl delete -f $(helper_url letsencrypt.yaml) || true
    # helm delete --purge cert-manager --tls
    # kubectl delete \
    #     -f https://raw.githubusercontent.com/jetstack/cert-manager/release-${ver%.*}/deploy/manifests/00-crds.yaml
    # kubectl delete serviceaccount cert-manager-cainjector --namespace cert-manager
    # kubectl delete serviceaccount cert-manager --namespace cert-manager
    kubectl apply \
        --validate=false \
        -f https://raw.githubusercontent.com/jetstack/cert-manager/release-${ver%.*}/deploy/manifests/00-crds.yaml

    helm upgrade --install \
        --wait \
        --version ${ver} \
        --set ingressShim.defaultIssuerName=letsencrypt-prod \
        --set ingressShim.defaultIssuerKind=ClusterIssuer \
        --namespace cert-manager \
        cert-manager \
        jetstack/cert-manager \
        --tls

    kubectl apply -f $(helper_url letsencrypt.yaml)
}

function ensure_sentry() {
    helm repo add incubator https://kubernetes-charts-incubator.storage.googleapis.com/
    helm repo update
    helm upgrade --install \
        --wait \
        --set sentry.dsn=${SENTRY_K8S_DSN} \
        --namespace kube-system \
        sentry-kubernetes \
        incubator/sentry-kubernetes \
        --tls
}

function launch_review_cluster() {
    if gcloud container clusters get-credentials ${CLUSTER_NAME}; then
        echo "Review cluster already exists"
        return
    fi

    tmp=$(mktemp -d -p "${TMP}" helm-XXXXXXXX)
    HELMHOME=$(helm home)
    CACERT=${HELMHOME}/ca.pem
    CAKEY=${tmp}/ca.key.pem
    echo ${HELM_CA_KEY_PEM} | base64 -d > ${CAKEY}

    openssl genrsa -out ${tmp}/tiller.key.pem 4096
    openssl req -key ${tmp}/tiller.key.pem -batch -new -sha256 -out ${tmp}/tiller.csr.pem -subj "/"
    openssl x509 -req -CA ${CACERT} -CAkey ${CAKEY} -CAcreateserial -in ${tmp}/tiller.csr.pem -out ${tmp}/tiller.cert.pem -days 10000

    gcloud container clusters create \
        --machine-type=${GCP_NODE_TYPE_REVIEW} \
        --num-nodes=${GCP_NODES_REVIEW} \
        --addons=HorizontalPodAutoscaling \
        --enable-autorepair \
        ${CLUSTER_NAME}

    kubectl create -f $(helper_url rbac-config.yaml)
    helm init \
        --tiller-tls \
        --tiller-tls-cert ${tmp}/tiller.cert.pem \
        --tiller-tls-key ${tmp}/tiller.key.pem \
        --tls-ca-cert ${HELMHOME}/ca.pem \
        --tiller-tls-verify \
        --service-account=tiller

    # Cheap substitute for --wait, since --wait doesn't work with TLS
    echo "Waiting for tiller pod to come online"
    until helm ls --tls >/dev/null 2>&1; do
        sleep 1
    done

    kubectl create -f $(helper_url gitlab.yaml)

    EXTERNAL_IP=$(host foo.dev.flashbacksrs.com | awk '/has address/ { print $4; exit }')

    OLD_RULE=$(gcloud compute forwarding-rules list | grep ${EXTERNAL_IP} | cut -d' ' -f1 || true)
    if [ -n "$OLD_RULE" ]; then
        gcloud compute forwarding-rules delete "$OLD_RULE" --region ${REGION} --quiet || true
    fi
}

# full_deploy is meant to simplify the 'review' and 'production' stages, by
# allowing 'script' to be a single command. full_deploy takes an optional list
# of arguments to pass to the helm upgrade command, meant primarily for custom
# values passed via --set and related flags.
function full_deploy() {
    announce install_dependencies
    announce setup_helm_keys
    announce gcloud_setup
    announce ensure_cluster
    announce install_tiller
    announce setup_helm_repo
    announce ensure_ingress
    announce ensure_cert_manager
    announce ensure_sentry
    announce remove_stale_deployments
    announce deploy "$@"
    announce persist_environment_url
}

function deployment_name() {
    if [ "$CI_ENVIRONMENT_SLUG" == "production" ]; then
        echo ${PRODUCTION_DEPLOYMENT_NAME}
        return
    fi
    echo ${CI_ENVIRONMENT_SLUG}
}

function deploy() {
    echo "Deploying..."

    if [ "${1}x" == "--local-chartx" ]; then
        shift
        chart="./flashback"
    else
        chart="flashback-*.tgz"
        helm fetch flashback/flashback
    fi

    env_slug=$( echo ${CI_ENVIRONMENT_SLUG//-/_} | tr -s  '[:lower:]'  '[:upper:]' )

    couchdb_persist=true
    if [ "${env_slug}" != "PRODUCTION" ]; then
        couchdb_persist=false
        REPLICAS="--set webClient.replicaCount=1 --set syslog.replicaCount=1 --set server.replicaCount=1"
    fi

    helm upgrade --install \
        --wait \
        --set release="$CI_ENVIRONMENT_SLUG" \
        --set registry.username="$FBDEPLOY_USER" \
        --set registry.password="$FBDEPLOY_PASSWORD" \
        --set auth.host=$(extract_host "$PRODUCTION_URL") \
        --set ingress.url="$CI_ENVIRONMENT_URL" \
        --set baseURL="$CI_ENVIRONMENT_URL" \
        --set gcp.project="$GCP_PROJECT" \
        --set backup.gcp_access_key="$GCP_BACKUP_ACCESS_KEY" \
        --set-string facebook.id="$FLASHBACK_FACEBOOK_ID" \
        --set gtm.token="$FLASHBACK_GTM_TOKEN" \
        --set-string facebook.secret="$FLASHBACK_FACEBOOK_SECRET" \
        --set couchdb.clusterSize="$(default 1 COUCHDB_CLUSTER_SIZE ${env_slug})" \
        --set couchdb.adminPassword="$(default '' COUCHDB_ADMIN_PASSWORD ${env_slug})" \
        --set couchdb.cookieAuthSecret="$(default '' COUCHDB_COOKIE_SECRET ${env_slug})" \
        --set couchdb.persistentVolume.enabled=${couchdb_persist} \
        --set log.level="${LOG_LEVEL}" \
        --set log.loggly_disabled="${FLASHBACK_LOGGLY_DISABLED}" \
        --set log.loggly_token="${LOGGLY_TOKEN}" \
        --set log.sentry_disabled="${FLASHBACK_SENTRY_DISABLED}" \
        --set log.sentry_dsn="${SENTRY_DSN}" \
        --set log.sentry_frontend_dsn="${SENTRY_FRONTEND_DSN}" \
        --namespace="$(deployment_name)" \
        ${REPLICAS:-} \
        "$@" \
        "$(deployment_name)" \
        $chart \
        --tls
}

function remove_stale_deployments() {
    if [ "${CI_ENVIRONMENT_SLUG}" == "production" ]; then
        return
    fi
    echo "Checking for stale deployment ..."
    if helm status "${CI_ENVIRONMENT_SLUG}" --tls 2>/dev/null | grep -E '^STATUS: (PENDING_INSTALL|FAILED)$' > /dev/null; then
        delete
    fi
}

function install_dependencies() {
    apk add -U -u openssl curl tar gzip bash ca-certificates git openssh python bind-tools
    curl -s -f -L -o /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub
    curl -s -f -L -O https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.28-r0/glibc-2.28-r0.apk
    apk add glibc-2.28-r0.apk
    rm glibc-2.28-r0.apk

    curl -s -f -L "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
    mv linux-amd64/helm /usr/bin/
    mv linux-amd64/tiller /usr/bin/
    helm version --client
    tiller -version

    curl -s -f -L -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
    chmod +x /usr/bin/kubectl
    kubectl version --client

    curl -s -f -L "https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-228.0.0-linux-x86_64.tar.gz" -o - | tar -xz
    ./google-cloud-sdk/install.sh --usage-reporting=false --path-update=true
    export PATH=$(pwd)/google-cloud-sdk/bin:${PATH}
}

function setup_docker() {
    if ! docker info &>/dev/null; then
        if [ -z "$DOCKER_HOST" -a "$KUBERNETES_PORT" ]; then
            export DOCKER_HOST='tcp://localhost:2375'
        fi
    fi
}

function build() {
    if [[ -n "$CI_REGISTRY_USER" ]]; then
        echo "Logging to GitLab Container Registry with CI credentials..."
        docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
        echo ""
    fi

    DOCKERFILE=${1:-Dockerfile}

    echo "Building Dockerfile-based application..."
    docker build \
        --build-arg CI_COMMIT_REF_NAME=$CI_COMMIT_REF_NAME \
        --build-arg CI_COMMIT_SHA=$CI_COMMIT_SHA \
        --build-arg GO_BUILD_TAGS=${GO_BUILD_TAGS:-} \
        -t "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG" \
        -f "$DOCKERFILE" \
        .

      echo "Pushing to GitLab Container Registry..."
      docker push "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"

      docker tag "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG" "$CI_APPLICATION_REPOSITORY:latest"
      docker push "$CI_APPLICATION_REPOSITORY:latest"
      echo ""
}

function setup_helm_keys() {
    mkdir -p $(helm home)
    echo ${helm_ca_pem} | base64 -d > $(helm home)/ca.pem
    echo ${helm_cert_pem} | base64 -d > $(helm home)/cert.pem
    echo ${helm_key_pem} | base64 -d > $(helm home)/key.pem
}

function install_tiller() {
    echo "Checking Tiller..."
    helm init --upgrade
    kubectl rollout status -n "kube-system" -w "deployment/tiller-deploy"
    if ! helm version --tls; then
        echo "Failed to init Tiller."
        return 1
    fi
    echo ""
}

function setup_helm_repo() {
    echo "Configuring helm repos..."
    mkdir -p ${HOME}/.ssh
    echo ${chart_ssh_key} | base64 -d > ${HOME}/.ssh/id_rsa
    chmod og-rxw ${HOME}/.ssh/id_rsa
    echo "|1|dZo2e97FFd0jmtMLn8Ixl/qRvhs=|m5ZlCRAqsSOqVO7SHKzHfX3uDpY= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=" > ${HOME}/.ssh/known_hosts
    helm init --client-only
    if [[ -z SKIP_PRIVATE_HELM_REPO ]]; then
        helm plugin install https://github.com/diwakar-s-maurya/helm-git
        helm repo add flashback gitlab://FlashbackSRS/priv/charts-repo:master
        helm repo update
    fi
}

function dast() {
    export CI_ENVIRONMENT_URL=$(cat environment_url.txt)

    mkdir /zap/wrk/
    /zap/zap-baseline.py -J gl-dast-report.json -t "$CI_ENVIRONMENT_URL" || true
    cp /zap/wrk/gl-dast-report.json .
}

function performance() {
    export CI_ENVIRONMENT_URL=$(cat environment_url.txt)

    mkdir gitlab-exporter
    wget -O gitlab-exporter/index.js https://gitlab.com/gitlab-org/gl-performance/raw/10-5/index.js

    mkdir sitespeed-results

    if [ -f .gitlab-urls.txt ]
    then
        sed -i -e 's@^@'"$CI_ENVIRONMENT_URL"'@' .gitlab-urls.txt
        docker run --shm-size=1g --rm -v "$(pwd)":/sitespeed.io sitespeedio/sitespeed.io:6.3.1 --plugins.add ./gitlab-exporter --outputFolder sitespeed-results .gitlab-urls.txt
    else
        docker run --shm-size=1g --rm -v "$(pwd)":/sitespeed.io sitespeedio/sitespeed.io:6.3.1 --plugins.add ./gitlab-exporter --outputFolder sitespeed-results "$CI_ENVIRONMENT_URL"
    fi

    mv sitespeed-results/data/performance.json performance.json
}

function persist_environment_url() {
    echo $CI_ENVIRONMENT_URL > environment_url.txt
}

function delete() {
    name="$CI_ENVIRONMENT_SLUG"
    echo "Deleting installation ${name} ..."

    if [[ -n "$(helm ls -q "^$name$" --tls)" ]]; then
        helm delete "$name" --purge --tls
        # This following hack is necessary because removing CouchDB doesn't
        # usually work for some reason. See https://github.com/kubernetes/charts/issues/5605
        while [ -n "$(kubectl get all --namespace $CI_ENVIRONMENT_SLUG | grep -v 'No resources found.')"  ]; do
            sleep 1
            kubectl delete all --all --namespace "$CI_ENVIRONMENT_SLUG"
        done
    fi
}

function cordova_build() {
    set -x
    echo ${google_cloud_key} | base64 -d > ./google_api.p12
    mkdir -p $(dirname $SRCDIR)
    ln -s $CI_PROJECT_DIR $SRCDIR
    cd $SRCDIR
    go generate ./...
    if [[ "$CI_COMMIT_REF_NAME" == "master" ]]; then
        FLASHBACK_PROD=1 make android
    else
        make android
        mkdir -p apk
        cp ./platforms/android/app/build/outputs/apk/debug/app-debug.apk ./apk/flashback-debug-${CI_COMMIT_SHA:0:12}.apk
    fi
}

function google_play_publish() {
    oldver=$(git show $CI_COMMIT_SHA:cordova.xml | grep '<widget id' | sed -e 's/<widget.* version="\([^"]*\)".*>/\1/')
    newver=$(cat cordova.xml | grep '<widget id' | sed -e 's/<widget.* version="\([^"]*\)".*>/\1/')
    if version_gt $newver $oldver; then
        echo "${newver} is newer than ${oldver}, deploying to Google Play"
        echo ${google_play_publish_key} | base64 -d > ./google_play.json
        ./publisher ./google_play.json flashback-release.apk
    fi
}

function post_test() {
    dir="${1:-$(pwd)}"
    tmp=$(mktemp -p "${TMP}" post_test-XXXXXXX)

    SAFEWORD="NO"MERGE # construct the safeword without triggering the test
    FAIL=0

    if git -C "${dir}" -c color.grep=always grep -n ${SAFEWORD} > $tmp; then
        echo "${SAFEWORD} found; merging not permitted"
        cat ${tmp}
        echo
        FAIL=1
    fi

    ANCESTOR=$(ancestor "${dir}")

    if ! git -C "${dir}" -c color.diff=always log ${ANCESTOR}..HEAD --exit-code --grep ${SAFEWORD} > $tmp; then
        echo "${SAFEWORD} found in commit message; merging not permitted"
        cat ${tmp}
        echo
        FAIL=1
    fi

    if ! git -C "${dir}" -c color.diff=always log ${ANCESTOR}..HEAD --exit-code -i --grep "^debug$" > $tmp; then
        echo "'debug' commit message found; merging not permitted"
        cat ${tmp}
        echo
        FAIL=1
    fi

    if ! git -C "${dir}" -c color.diff=always log ${ANCESTOR}..HEAD --exit-code -i --grep "^wip\b" > $tmp; then
        echo "'WIP' commit message found; merging not permitted"
        cat ${tmp}
        echo
        FAIL=1
    fi

    exit ${FAIL}
}
